#**************************************************
# implementation
#**************************************************
import pathlib
import re
from luigi.task import flatten

from jinja2 import Template

# [yc] change the template, remove the f string type
tmpl_tasks = '''
import d6tflow
import luigi

{% for task in tasks -%}

class {{task.name}}({{task.class}}):
    persist={{task.obj.persist}}
    {% for param in task.params -%}
    {{param.name}}={{param.class}}(default={{param.default}})
    {% endfor %}
{% endfor %}
'''

# [yc] use regex to clean the class name
def get_class_name(string):
    pattern = r"'([\w.]+)'"
    return re.findall(pattern, string)[0]

class FlowExport(object):
    
    def __init__(self, tasks, pipename, write_dir='.', write_filename_tasks = 'tasks_d6tpipe.py', write_filename_run = 'run_d6tpipe.py'):
        # todo NN: copy = False # copy task output to pipe
        # todo NN: run = False # run flow
        if not isinstance(tasks, (list,)):
            tasks = [tasks] 
        
        # todo: init all params => self.param=param
        write_dir = pathlib.Path(write_dir)
        
        # [yc] init the params
        self.tasks = tasks
        self.pipename = pipename
        self.write_dir = write_dir
        self.write_filename_tasks = write_filename_tasks
        self.write_filename_run = write_filename_run
    
    def traverse(self, t, path=None): # todo NN: move to d6tflow.utils
        if path is None: path = []
        path = path + [t]
        for node in flatten(t.requires()):
            if not node in path:
                path = self.traverse(node, path)
        return path
    
    def generate(self):
        # todo: 
        
        # [yc] create tasksPrint list to store the output
        tasksPrint = []
        
        for task_ in self.tasks:
            for task in self.traverse(task_):
                if getattr(task,'export',True):
                    taskPrint = {'name':task.__class__.__name__,
                                 'class':get_class_name(str(type(task).__mro__[1])),
                                 'obj':task}
                    
                    taskPrint['params'] = [{'name':param[0], 
                                            'class':get_class_name(str(param[1].__class__)),
                                            'default': repr(param[1]._default)} for param in task.get_params()]
                    tasksPrint.append(taskPrint)
        
        # [yc] change the tasksPrint list order to match output order
        tasksPrint[-1], tasksPrint[0:-1] = tasksPrint[0], tasksPrint[1:]

            # todo: how to get class name? type(task).__mro__[1] => d6tflow.tasks.TaskCache BUT str(type(task).__mro__[1]) => Out[52]: "<class 'd6tflow.tasks.TaskCache'>"
            # get d6tflow class if class was inherited: type(task).__mro__.frst('contains','d6tflow.tasks.','d6tflow2.tasks.')

        template = Template(tmpl_tasks)
        # template.render(task=tasksPrint) # => todo: save to self.write_dir/self.write_filename_tasks
        
        # [yc] save the tasks
        task_dir = self.write_dir.joinpath(self.write_filename_tasks)
        file = open(task_dir, 'w')
        file.write(template.render(tasks=tasksPrint))
        file.close()

        # [yc] update the template
        tmpl_run = '''
import {{self_.write_filename_tasks[:-3]}}
import d6tflow.pipes

d6tflow.pipes.init('{{self_.pipename}}') # yes but user can override if they want to save to their own pipe
d6tflow.pipes.get_pipe('{{self_.pipename}}').pull()

# to load data see https://d6tflow.readthedocs.io/en/latest/tasks.html#load-output-data
# available tasks are shown in {{self_.write_filename_tasks}}

'''
        # [yc] save the runs
        template = Template(tmpl_run) # => todo: save to self.write_dir/self.write_filename_run
        run_dir = self.write_dir.joinpath(self.write_filename_run)
        file = open(run_dir, 'w')
        file.write(template.render(self_ = self))
        file.close()
        
    def push(self): # pipename.push()
        raise NotImplementedError()
    
    def test(self): # test if target user can run all
        raise NotImplementedError()

    def complete(self): # check if all tasks complete
        raise NotImplementedError()
    
